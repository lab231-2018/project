import { Component, OnInit } from '@angular/core';
import Student from '../entity/student';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AuthenticationService } from '../service/authentication.service';
import { StudentService } from '../service/student-service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import User from '../entity/user';
import { ErrorStateMatcher } from '../../../node_modules/@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {
  myForm: FormGroup;
  matcher = new MyErrorStateMatcher();

  student: Student;
  model: Student = new Student();

  user: User;
  userModel: User = new User();

  authService: AuthenticationService;

  fb: FormBuilder = new FormBuilder();
  passwordFormGroup: FormGroup;
  validation_messages = {
    'password': [
      { type: 'required', message: 'Old password is required' }
    ],
    'newPassword': [
      { type: 'required', message: 'New password is required' }
    ],
    'repeatPassword': [
      { type: 'required', message: 'Please confirm new password' }
    ]
    // 'studentId': [
    //   { type: 'required', message: 'student id is required' },
    //   { type: 'maxlength', message: 'student is is too long' },
    //   { type: 'pattern', message: 'Please enter number' }
    // ],
    // 'name': [
    //   { type: 'required', message: 'the name is required' }
    // ],
    // 'surname': [
    //   { type: 'required', message: 'the surname is required' }
    // ],
    // 'image': [
    // ],
    // 'date': [
    //   { type: 'required', message: 'Date of birth is required' }
    // ],
    // 'email': [
    //   { type: 'required', message: 'Email is required' },
    //   { type: 'pattern', message: 'Please enter correctly' }
    // ],
    // 'password': [
    //   { type: 'required', message: 'Password is required' }
    // ]
  };
  // tslint:disable-next-line:max-line-length
  constructor(private studentService: StudentService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) {
    this.myForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirmPassword: ['']
    }, { validator: this.checkPasswords });

  }
  //this.http.get<any>.(environment.studentApi1,{id:})
  ngOnInit(): void{
    this.route.params.subscribe((params: Params) => {
      this.authService.getCurrentUser()
        .subscribe((inputUser : User) => this.user = inputUser);
      });
    
      this.passwordFormGroup = this.fb.group({
        password: [this.user.password, Validators.required],
        newPassword: [this.user.newPassword, Validators.required],
        repeatPassword: [this.user.repeatPassword, Validators.required]
      });
  }
  // ngOnInit() {
  //   this.route.params
  //     .subscribe((params: Params) => {
  //       this.studentService.getStudent(+params['id'])
  //         .subscribe((inputStudent: Student) => this.student = inputStudent);
  //     });
  // this.sf = this.fb.group({
  //   studentId: [this.student.studentId, Validators.compose([Validators.required,
  //   Validators.maxLength(10), Validators.pattern('[0-9]+')])],
  //   name: [this.student.name, Validators.required],
  //   surname: [this.student.surname, Validators.required],
  //   image: [this.student.image],
  //   // tslint:disable-next-line:max-line-length
  //   date: [this.student.date, Validators.required],
  //   // tslint:disable-next-line:max-line-length
  // tslint:disable-next-line:max-line-length
  //   email: [this.student.email, Validators.compose([Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')])],
  //   password: [this.student.password, Validators.required],
  // });
  // }

  // onSubmit() {
  //   this.userModel = this.passwordFormGroup.value;
  //   console.log(this.userModel);
  //   this.model.id = this.student.id;
  //   this.studentService.updateStudent(this.model)
  //     .subscribe((student) => {
  //       this.router.navigate(['detail', student.id]);
  //     }, (error) => {
  //       alert('could not update student');
  //     });
  // }

  onSubmit() {
    this.userModel = this.passwordFormGroup.value;
    if(this.authService.hasRole('STUDENT')){
      this.studentService.changePassword(this.model);
    }
  }
  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

}
