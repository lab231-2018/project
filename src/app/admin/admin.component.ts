import { Component, OnInit } from '@angular/core';
import { Admin } from '../entity/admin';
import { Alert } from 'selenium-webdriver';
import { MyNavComponent } from '../my-nav/my-nav.component';
import { NavbarServiceService } from '../navbar-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  model: Admin = new Admin();
  password: string;

  constructor(public nav: NavbarServiceService, private route: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    if ((this.model.password) == 12345) {
      alert("You are admin");
      this.nav.adminLogin();
      this.nav.Login();
      this.nav.setImage("https://png.pngtree.com/element_origin_min_pic/16/09/25/1757e7970ba4bb7.jpg");
      this.route.navigate(['/homepage']);
    }
  }


}
