import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { StudentService } from '../service/student-service';
import Student from '../entity/student';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  check : boolean = false;
  student : Student;
  defaultImageUrl = 'assets/images/camt.jpg';
  constructor(private route:ActivatedRoute,private studentService:StudentService,private authService: AuthenticationService) { }

  ngOnInit() {
    this.route.params
    .subscribe((params: Params) => {
      this.studentService.getStudent(params['id'])
        .subscribe((inputStudent: Student) => this.student = inputStudent);
    });
  }

  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

  get user() {
    // return this.authService.getCurrentUser();
    return this.student = this.authService.getCurrentUser();
  }

}
