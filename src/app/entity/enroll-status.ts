export class EnrollStatus {
    id: number;
    idAct: number;
    idStudent: number;
    name: string;
    location: string;
    periodOfRegis: string;
    date: string;
    hostTeacher: string;
    description: string;
    image: string;
    status : string;
}
