export default class Student {
  id: number;
  studentId: string;
  name: string;
  surname: string;
  date: string;
  image: string;
  email: string;
  password: string;
}
