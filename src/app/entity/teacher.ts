export default class Teacher {
    id: number;
    name: string;
    surname: string;
    image: string;
    email: string;
    password: string;
}
