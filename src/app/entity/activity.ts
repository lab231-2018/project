import Student from "./student";
import Teacher from "./teacher";

export default class Activity {
        id: number;
        name: string;
        location: string;
        periodOfRegis: string;
        date: string;
        lecturer: Teacher;
        description: string;
        image: string;
        studentWaitingList: string[];
        studentEnroll: string[];
        comment: string[];

}
