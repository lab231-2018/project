export default class User {
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
    newPassword: string;
    repeatPassword: string;
}