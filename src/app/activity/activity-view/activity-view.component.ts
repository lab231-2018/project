import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-activity-view',
  templateUrl: './activity-view.component.html',
  styleUrls: ['./activity-view.component.css']
})
export class ActivityViewComponent implements OnInit {
  activity: Activity;
  constructor(private route: ActivatedRoute, private activityService: ActivityService) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity: Activity) => this.activity = inputActivity);
      });
  }


}