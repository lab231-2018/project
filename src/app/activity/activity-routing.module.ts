import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';
import { ListComponent } from '../activity/list/list.component';
import { ActivityAddComponent } from '../activity/activity-add/activity-add.component';
import { ActivityViewComponent } from './activity-view/activity-view.component';
import { ActivityEditComponent } from './activity-edit/activity-edit.component';
import { CommentComponent } from './comment/comment.component';
import { ApproveComponent } from './approve/approve.component';

const activityRoutes: Routes = [
    { path: 'add-activity', component: ActivityAddComponent },
    { path: 'act-detail/:id', component: ActivityViewComponent },
    { path: 'act-edit/:id', component: ActivityEditComponent },
    { path: 'comment/:id' , component: CommentComponent},
    { path: 'approve' , component:ApproveComponent}
    { path: 'activitys/comment/:id' , component: CommentComponent}
];
@NgModule({
    imports: [
        RouterModule.forRoot(activityRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ActivityRoutingModule{}