import { Component, OnInit } from '@angular/core';
import Activity from 'src/app/entity/activity';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivityService } from 'src/app/service/activity-service';
import { Router } from '@angular/router';
import { TeacherService } from '../../service/teacher.service';
import Teacher from '../../entity/teacher';

@Component({
  selector: 'app-activity-add',
  templateUrl: './activity-add.component.html',
  styleUrls: ['./activity-add.component.css']
})
export class ActivityAddComponent implements OnInit {
  model: Activity = new Activity();
  fb: FormBuilder = new FormBuilder();
  af: FormGroup; // af stand for activity form
  activity: Activity;
  activitys: Activity[];
  teachers: Teacher[];
  validation_messages = {
    'id': [
      { type: 'required', message: 'activity id is required' }
    ],
    'name': [
      { type: 'required', message: 'activity name is required' }
    ],
    'location': [
      { type: 'required', message: 'activity location is required' }
    ],
    'description': [
      { type: 'required', message: 'activity description is required' }
    ],
    'date': [
      { type: 'required', message: 'activity date is required' }
    ],
    'hostTeacher': [
      { type: 'required', message: 'teacher name is required' }
    ],
    'periodOfRegis': [
      { type: 'required', message: 'period of registration is required' },
      { type: 'pettern', message: 'Please enter correctly' }
    ],
    'image': []
  };
  constructor(private activityService: ActivityService,
   private router: Router, private teacher: TeacherService) { }

  ngOnInit(): void {
    this.af = this.fb.group({
      id: [null, Validators.required],
      name: [null, Validators.required],
      location: [null, Validators.required],
      date: [null, Validators.required],
      // tslint:disable-next-line:max-line-length
      periodOfRegis: [null, Validators.compose([Validators.required, Validators.pattern('([01]?[0-9]|2[0-3]).[0-5][0-9][--.]([01]?[0-9]|2[0-3]).[0-5][0-9]')])],
      lecturer: [null, Validators.required],
      description: [null, Validators.required],
      image: [null]
    });

    this.teacher.getTeachers().subscribe(teachers => {
      this.teachers = teachers;
    });
  }
  onSubmit() {
    this.model = this.af.value;
    this.model.lecturer = this.af.value.lecturer;
    this.model.id = this.activityService.getActivitys.length+1;
    console.log(this.model);
    this.activityService.saveActivity(this.model)
      .subscribe((activity) => {
        this.router.navigate(['act-detail', activity.id]);
      }, (error) => {
        alert('could not save activity');
      });
  }

  // teachers: string[] = [
  //   'Mr.A' ,'Mr.B','Mr.C'
  // ];

}
