import { Component, OnInit, ViewChild } from '@angular/core';
import Student from '../../entity/student';
import { MatPaginator, MatSort } from '@angular/material';
import { StudentTableDataSource } from '../../students/student-table/student-table-datasource';
import { BehaviorSubject } from 'rxjs';
import { StudentService } from '../../service/student-service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthenticationService } from '../../service/authentication.service';

@Component({
  selector: 'app-approve',
  templateUrl: './approve.component.html',
  styleUrls: ['./approve.component.css']
})
export class ApproveComponent implements OnInit {
  model: Student = new Student();
  student : Student;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: StudentTableDataSource;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'surname', 'dob', 'image','accept', 'reject'];
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  loading: boolean;
  id: number;
  
  constructor(private studentService: StudentService,
      private router: Router,
      private route : ActivatedRoute,
      private authService : AuthenticationService,) { }
  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => this.id = +params['id']);
    this.loading = true;
    // if(this.hasRole('ADMIN')){
    // this.studentService.getRequestStudents()
    //   .subscribe(students => {
    //     this.dataSource = new StudentTableDataSource(this.paginator, this.sort);
    //     this.dataSource.data = students;
    //     this.students = students;
    //     this.filter$ = new BehaviorSubject<string>('');
    //     this.dataSource.filter$ = this.filter$;
    //     setTimeout(() => {
    //       this.loading = false;
    //     }, 500);
    //   }
    //   );
    // }else if(this.hasRole('LECTURER')){
      this.studentService.getStudentByActivityId(8)
      .subscribe(students => {
        console.log(students);
        this.dataSource = new StudentTableDataSource(this.paginator, this.sort);
        this.dataSource.data = students;
        this.students = students;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        setTimeout(() => {
          this.loading = false;
        }, 500);
      }
      );
    }
  
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  params : Params;
  num : number = 3;

  accept(student: Student) {
    this.studentService.getStudent(student.id)
    .subscribe((inputStudent: Student) => this.student = inputStudent);

    // console.log(this.studentService.getStudent(this.params['id']));
    // console.log(this.studentService.getStudent(this.params[this.num]));
    // console.log(this.studentService.getStudent(this.id));
    // console.log(this.studentService.getStudent(+2))
    // this.route.params
    //   .subscribe((params: Params) => {
    //     this.studentService.approveStudent(params['id'], student)
    //       .subscribe((inputStudent: Student) => this.student = inputStudent);
    //   });

    this.router.navigate(['/homepage']);
  }

  hasRole(role: string) {
    return this.authService.hasRole(role);
  }
  
  reject(student: any) {
    alert('You reject');
    this.studentService.deleteStudent(student);
    this.router.navigate(['/homepage']);
  }

}
