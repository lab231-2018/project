import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Activity from 'src/app/entity/activity';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-activity-edit',
  templateUrl: './activity-edit.component.html',
  styleUrls: ['./activity-edit.component.css']
})
export class ActivityEditComponent implements OnInit {
  activity: Activity;
  model: Activity = new Activity();
  fb: FormBuilder = new FormBuilder();
  af: FormGroup; // af stand for activity form
  validation_messages = {
    'id': [
      { type: 'required', message: 'activity id is required' }
    ],
    'name': [
      { type: 'required', message: 'activity name is required' }
    ],
    'location': [
      { type: 'required', message: 'activity location is required' }
    ],
    'description': [
      { type: 'required', message: 'activity description is required' }
    ],
    'date': [
      { type: 'required', message: 'activity date and time is required' }
    ],
    'hostTeacher': [
      { type: 'required', message: 'teacher name is required' }
    ],
    'periodOfRegis': [
      { type: 'required', message: 'period of registration is required' }
    ],
    'image': []
  };

  constructor(private route: ActivatedRoute, private activityService: ActivityService, private router: Router) { }

  ngOnInit(): void {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity: Activity) => this.activity = inputActivity);
      });
    this.af = this.fb.group({
      id: [this.activity.id, Validators.required],
      name: [this.activity.name, Validators.required],
      location: [this.activity.location, Validators.required],
      date: [this.activity.date, Validators.required],
      periodOfRegis: [this.activity.periodOfRegis, Validators.required],
      lecturer: [this.activity.lecturer, Validators.required],
      description: [this.activity.description, Validators.required],
      image: [this.activity.image]
    });
  }

  onSubmit() {
    this.model = this.af.value;
    this.model.id = this.activity.id;
    this.activityService.updateActivity(this.model)
      .subscribe((activity) => {
        this.router.navigate(['act-detail', activity.id]);
      }, (error) => {
        alert('could not save activity');
      });
  }


}
