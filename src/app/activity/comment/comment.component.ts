import { Component, OnInit } from '@angular/core';
import Activity from '../../entity/activity';
import { ActivatedRoute, Params, Router } from '../../../../node_modules/@angular/router';
import { ActivityService } from '../../service/activity-service';
import { FormBuilder, FormGroup, Validators } from '../../../../node_modules/@angular/forms';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  activity: Activity;
  model: Activity = new Activity();
  fb: FormBuilder = new FormBuilder();
  sf: FormGroup;
  constructor(private route: ActivatedRoute, private activityService: ActivityService, private router: Router) { }

  uploadEndPoint: string;
  uploadedUrl: string;
  ngOnInit() {
    this.uploadEndPoint = environment.uploadApi;
    this.sf = this.fb.group({
      image: [null],
      name: [null]
    });
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getCommentByActivityId(+params['id'])
          .subscribe((inputActivity: Activity) => this.activity = inputActivity);
      });
  }

  onSubmit() {
    this.model = this.sf.value;
    this.model.image = this.uploadedUrl;
    this.model.id = this.activity.id;
    this.activityService.updateActivity(this.model)
      .subscribe((activity) => {
        this.router.navigate(['activityTable']);
      }, (error) => {
        alert('could not save comment');
      });
  }

  processReturnFile(file: File, event: any): void {
    if (event.event.type === 'load') {
      console.log(event.event.target.response);
      this.uploadedUrl = event.event.target.response;
    }
  }

  isUploadedFile(): boolean {
    return this.uploadedUrl != null;
  }

}
