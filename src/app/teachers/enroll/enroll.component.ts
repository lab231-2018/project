import { Component, OnInit, ViewChild } from '@angular/core';
import Student from 'src/app/entity/student';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StudentService } from 'src/app/service/student-service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatPaginator, MatSort } from '@angular/material';
import { StudentTableDataSource } from 'src/app/students/student-table/student-table-datasource';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import { NavbarServiceService } from 'src/app/navbar-service.service';
import Activity from 'src/app/entity/activity';
import { AuthenticationService } from '../../service/authentication.service';

@Component({
  selector: 'app-enroll',
  templateUrl: './enroll.component.html',
  styleUrls: ['./enroll.component.css']
})
export class EnrollComponent implements OnInit {
  model: Student = new Student();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: StudentTableDataSource;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'surname', 'dob', 'image', 'email', 'password', 'accept', 'reject'];
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  loading: boolean;
  activity:Activity;
  constructor(private studentService: StudentService,
    authService : AuthenticationService
    , private nav: NavbarServiceService, private router: Router, private activityService:ActivityService, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity:Activity) => this.activity = inputActivity);
      })
    this.loading = true;
    this.studentService.getRequestStudents()
      .subscribe(students => {
        console.log('what we got',students);
        this.dataSource = new StudentTableDataSource(this.paginator, this.sort);
        this.dataSource.data = students;
        this.students = students;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        setTimeout(() => {
          this.loading = false;
        }, 500);
      }
      );
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  accept(student: any) {
    alert('You accept');
    this.studentService.saveStudent(student);
    this.studentService.deleteStudent(student);
    this.router.navigate(['/homepage']);
  }

  reject(student: any) {
    alert('You reject');
    this.studentService.deleteStudent(student);
    this.router.navigate(['/homepage']);
  }


}
