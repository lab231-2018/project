import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StudentService } from '../service/student-service';
import Student from '../entity/student';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NavbarServiceService } from '../navbar-service.service';
import { MatDialog } from '@angular/material';
import { AuthenticationService } from '../service/authentication.service';
@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent implements OnInit {
  defaultImageUrl = 'http://www.rammandir.ca/sites/default/files/default_images/defaul-avatar_0.jpg';
  student: Student;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  students$: Observable<Student[]> = this.studentService.getStudents();

  student$: Observable<Student> = this.studentService.getStudent(1);


  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.studentService.getStudent(+params['id']).subscribe((inputStudent: Student) => this.student = inputStudent);
    });
  }

  constructor(public router: Router,
    public dialog: MatDialog,
    private nav: NavbarServiceService,
    private breakpointObserver: BreakpointObserver,
    private studentService: StudentService,
    private route: ActivatedRoute
    , private authService: AuthenticationService) {

  }

  edit() {
    console.log(this.student.id);
    this.router.navigate(['edit', this.student.id]);
  }

  get user() {
    // return this.authService.getCurrentUser();
    return this.student = this.authService.getCurrentUser();
  }

  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

  updateProfile(){
    this.router.navigate(['updateProfile', this.student.id]);
  }

  Logout(){
    this.authService.logout();
  }


}
