import { Injectable } from "@angular/core";
import { ActivityTableComponent } from "./students/activity-table/activity-table.component";
import { AuthenticationService } from "./service/authentication.service";


@Injectable({
  providedIn: 'root'
})
export class NavbarServiceService {
  visibleStudent: boolean;
  visible: boolean;
  admin: boolean;
  noti:boolean;
  isLogin: boolean;

  displayedColumns;

  image: string;
  id : number;
  studentId: string;
  name: string;
  surname: string;
  date: string;
  email: string;
  password: string;

  act :ActivityTableComponent;

  setStudent(id : number,studentId : string,name : string,surname: string,date : string,image : string){
    this.id = id
    this.studentId = studentId
    this.name = name
    this.surname = surname
    this.date = date
    this.image = image
  }

  setTeacher(id : number,name : string,surname: string,image : string){
    this.id = id
    this.name = name
    this.surname = surname
    this.image = image
  }

  constructor(private authService:AuthenticationService) { this.visible = false;
  this.visibleStudent = false;
  this.image =   "https://coswafe.com/img/static/blank.png"
  this.isLogin = false
this.admin = false
}

  adminLogin(){
    this.admin = true;
  }


  hide() { this.visible = false; }

  show() { this.visible = true; }

  toggle() { this.visible = !this.visible; }

  Login(){
    this.isLogin = true;
  }

  Logout(){
    this.authService.logout();
    this.image = "https://coswafe.com/img/static/blank.png";
    this.isLogin = false;
    this.visible = false;
    this.visibleStudent = false;
    this.admin = false;
    this.setStudent(undefined,undefined,undefined,undefined,null,"https://coswafe.com/img/static/blank.png");
    this.setTeacher(null,null,null,"https://coswafe.com/img/static/blank.png");

  }

  showStudent() {
    this.visibleStudent = true;
  }
  

  setImage(image:string) {
    this.image = image;
   }

   setId(id : number){
     this.id = id;
   }

   
}