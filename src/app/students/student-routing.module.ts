import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';
import { NgModule, OnInit } from '@angular/core';
import { StudentTableComponent } from './student-table/student-table.component';
import { HomepageComponent } from '../homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { ActivityAddComponent } from '../activity/activity-add/activity-add.component';
import { AdminComponent } from '../admin/admin.component';
import { ActivityTableComponent } from './activity-table/activity-table.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ActivityEditComponent } from '../activity/activity-edit/activity-edit.component';
import { EnrollTableComponent } from './enroll-table/enroll-table.component';
import { EnrollComponent } from '../teachers/enroll/enroll.component';
import { NavbarServiceService } from '../navbar-service.service';
import { UpdateProfileComponent } from '../update-profile/update-profile.component';
import { ListComponent } from '../activity/list/list.component';

const StudentRoutes: Routes = [
    { path: 'homepage', component: HomepageComponent },
    { path: 'add', component: StudentsAddComponent },
    { path: 'detail/:id', component: StudentsViewComponent },
    { path: 'table', component: StudentTableComponent },
    { path: 'table/:id', component: StudentTableComponent },
    { path: 'login', component: LoginComponent },
    { path: 'activity', component: ActivityAddComponent },
    { path: 'admin', component: AdminComponent },
    { path: 'activityTable', component: ActivityTableComponent },
    { path: 'edit/:id', component: EditProfileComponent },
    { path: 'homepage/:id', component: HomepageComponent },
    { path: 'list', component: ActivityTableComponent },
    { path: 'act-edit/:id', component: ActivityEditComponent },
    { path: 'enrollTable', component: EnrollTableComponent },
    { path: 'admin', component: AdminComponent },
    { path: 'teacher-enroll/:id', component: EnrollComponent},
    { path: 'updateProfile/:id', component: UpdateProfileComponent },
    { path: 'approve',component: ListComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {
    constructor(private nav:NavbarServiceService){

    }

}
