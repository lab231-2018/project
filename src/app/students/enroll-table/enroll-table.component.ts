import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivityService } from '../../service/activity-service';
import { Router } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import Activity from '../../entity/activity';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from '../../service/authentication.service';
import Student from '../../entity/student';

@Component({
  selector: 'app-enroll-table',
  templateUrl: './enroll-table.component.html',
  styleUrls: ['./enroll-table.component.css'],
})
export class EnrollTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource<Activity>();
  selection = new SelectionModel<Activity>(true, []);
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'location', 'description', 'periodOfRegis', 'date', 'hostTeacher', 'image','status'];
  activitys: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  loading: boolean;
  student: Student;

  constructor(private activityService: ActivityService, private router: Router,private authService:AuthenticationService) { }

  ngOnInit() {
    this.loading = true;
    this.student = this.user;
    this.activityService.getActivityByStundetId(this.student.id)
      .subscribe(activitys => {
        this.dataSource = new MatTableDataSource<Activity>();
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.loading = false;
        }, 500);
      });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  get user() {
    return this.student = this.authService.getCurrentUser();
  }

}
