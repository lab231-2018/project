import { Component, OnInit } from '@angular/core';
import Student from '../../entity/student';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { StudentService } from '../../service/student-service';
import Activity from 'src/app/entity/activity';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  student: Student;
  model: Student = new Student();
  fb: FormBuilder = new FormBuilder();
  sf: FormGroup;

  validation_messages = {
    'studentId': [
      { type: 'required', message: 'student id is required' },
      { type: 'maxlength', message: 'student is is too long' },
      { type: 'pattern', message: 'Please enter number' }
    ],
    'name': [
      { type: 'required', message: 'the name is required' }
    ],
    'surname': [
      { type: 'required', message: 'the surname is required' }
    ],
    'image': [
    ],
    'date': [
      { type: 'required', message: 'Date of birth is required' }
    ],
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Please enter correctly' }
    ],
    'Password': [
      { type: 'required', message: 'Password is required' }
    ]
  };
  constructor(private studentService: StudentService, private router: Router, private route: ActivatedRoute) {

  }
  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.studentService.getStudent(+params['id'])
          .subscribe((inputStudent: Student) => this.student = inputStudent);
      });
    this.sf = this.fb.group({
      studentId: [this.student.studentId, Validators.compose([Validators.required,
      Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      name: [this.student.name, Validators.required],
      surname: [this.student.surname, Validators.required],
      image: [this.student.image],
      // tslint:disable-next-line:max-line-length
      date: [this.student.date, Validators.required],
      // tslint:disable-next-line:max-line-length
      email: [this.student.email, Validators.compose([Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')])],
      password: [this.student.password, Validators.required],
    });
  }

  onSubmit() {
    this.model = this.sf.value;
    console.log(this.model);
    this.model.id = this.student.id;
    this.studentService.updateStudent(this.model)
      .subscribe((student) => {
        this.router.navigate(['detail', student.id]);
      }, (error) => {
        alert('could not update student');
      });
  }

}
