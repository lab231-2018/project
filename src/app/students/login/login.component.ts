import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { StudentService } from '../../service/student-service';
import { Router, Params } from '@angular/router';
import Student from '../../entity/student';
import { NavbarServiceService } from '../../navbar-service.service';
import { TeacherService } from '../../service/teacher.service';
import Teacher  from '../../entity/teacher';
import { ActivityTableComponent } from '../activity-table/activity-table.component';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../../service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  loginStudentCheck: boolean
  loginTeacherCheck: boolean
  returnUrl: string;
  isError = false;
  home:boolean;

  addressForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required]
  });

  ngOnInit(){
    //reset login status
this.authenService.logout();
this.isError = false;
this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
if(this.returnUrl === '/'){
  this.home = true;
}else{
  this.home = false;
}
console.log('return url'+this.returnUrl);
  }
  
  teachers: Teacher;
  student: Student;
  model: Student = new Student();
  modelTeacher: Teacher = new Teacher();
  act:ActivityTableComponent;

  constructor(private fb : FormBuilder,private teacherService:TeacherService,
    private studentService: StudentService, private route: ActivatedRoute
     ,private router: Router,public nav: NavbarServiceService,
     private authenService : AuthenticationService) {
  this.loginStudentCheck = false;
  this.loginTeacherCheck = false;
  }

  onSubmit() {
    this.authenService.login(this.model.email,this.model.password)
    .subscribe(data =>{
      // this.nav.showStudent();
      localStorage.setItem('test', data);
      console.log(localStorage);
      
      this.nav.Login();
      this.isError = false;
      if(this.hasRole('STUDENT')){
      this.router.navigate(['/activityTable']);
      }else if(this.hasRole('ADMIN')){
        this.router.navigate(['/table']);
      }else if(this.hasRole('LECTURER')){
        this.router.navigate(['/activityTable']);
      }

    },error =>{
      this.isError = true;
    })
    console.log(this.model);
  }

  hasRole(role: string) {
    return this.authenService.hasRole(role);
  }

loginStudent(){
  this.loginStudentCheck = true;
  this.loginTeacherCheck = false;
}

loginTeacher(){
  this.loginTeacherCheck = true;
  this.loginStudentCheck = false;
}

}
