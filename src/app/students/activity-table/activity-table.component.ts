import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from '../../entity/activity';
import { BehaviorSubject } from '../../../../node_modules/rxjs';
import { ActivityService } from '../../service/activity-service';
import { SelectionModel } from '../../../../node_modules/@angular/cdk/collections';
import { Router } from '@angular/router';
import { NavbarServiceService } from '../../navbar-service.service';
import { AuthenticationService } from '../../service/authentication.service';
import Teacher from '../../entity/teacher';

@Component({
  selector: 'app-activity-table',
  templateUrl: './activity-table.component.html',
  styleUrls: ['./activity-table.component.css']
})
export class ActivityTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource<Activity>();
  selection = new SelectionModel<Activity>(true, []);
  displayedColumns;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  teacher : Teacher;

  change() {
    if (this.hasRole('STUDENT')) {
      this.displayedColumns = ['name', 'location', 'description', 'periodOfRegis', 'date', 'hostTeacher', 'image', 'add', 'comment'];
    } else {
      // tslint:disable-next-line:max-line-length
      this.displayedColumns = ['name', 'location', 'description', 'periodOfRegis', 'date', 'hostTeacher', 'image', 'edit', 'comment','approve'];
    }
  }
  

  activitys: Activity[];
  filterValue: string;
  filter: string;
  filter$: BehaviorSubject<string>;
  loading: boolean;
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  // tslint:disable-next-line:max-line-length
  constructor(private activityService: ActivityService, private router: Router,
     private nav: NavbarServiceService ,private authService : AuthenticationService) { }
  ngOnInit() {
    this.teacher = this.user;
    this.loading = true;
    console.log(this.teacher.id);
    if(this.hasRole('STUDENT')){
    this.activityService.getActivitys()
      .subscribe(activitys => {
        this.dataSource = new MatTableDataSource<Activity>();
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.sort = this.sort;
        this.change();
        setTimeout(() => {
          this.loading = false;
        }, 500);
      });
    }else{
      this.activityService.getActivityByLecturerId(this.teacher.id)
      .subscribe(activitys => {
        this.dataSource = new MatTableDataSource<Activity>();
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.sort = this.sort;
        this.change();
        setTimeout(() => {
          this.loading = false;
        }, 500);
      });
    }
    if (this.nav.visible) {
      this.applyFilter(this.nav.name);
    }
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  edit(activity: any) {
    this.activityService.getActivity(activity.id)
      // tslint:disable-next-line:no-shadowed-variable
      .subscribe((activity) => {
        this.router.navigate(['act-edit', activity.id]);
      }, (error) => {
        alert('could not save value');
      });
  }

  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

  add(activity: any) {
    // if(this.activityService.getActivityByStundetId(this.user.id).subscribe.){
      this.activityService.getActivityByStundetId(this.user.id)
      .subscribe(activitys => {
        this.activitys = activitys;
        for(let act of activitys){
          if(activity.name == act.name){
           alert("You already enroll this activity")
          }else{
            this.activityService.saveEnroll(activity.id)
          }
        }
      });
  }

  view(id: number) {
    console.log(this.activityService.getActivity(id));
    // this.activityService.getEnrolls()
    // this.router.navigate(['/teacher-enroll',id]);
  // add(activity: Activity) {
  //   if(this.activityService.check(activity.id)){
  //   this.activityService.saveEnroll(activity.id,this.nav.studentId)
  //   this.router.navigate(['/enrollTable']);
  //     }else{
  //       alert("Error");
  //     }
  }

  approve(activity: Activity) {
    this.router.navigate(['table', activity.id]);
  }

  comment(activity: Activity){
    this.activityService.getActivity(activity.id)
    .subscribe((activity) => {
      this.router.navigate(['comment', activity.id]);
    }, (error) => {
      alert('could not save value');
    });
  }

  get user() {
    return this.teacher = this.authService.getCurrentUser();
  }
}
