import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  // tslint:disable-next-line:max-line-length
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatCheckboxModule, MatSelectModule, MatDialog, MatAutocompleteModule, MatBadgeModule, MatDatepickerModule, MatNativeDateModule
} from '@angular/material';
import { MatInputModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { MatProgressSpinnerModule } from '@angular/material';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { StudentsRestImplService } from './service/students-rest-impl.service';
import { HomepageComponent } from './homepage/homepage.component';
import { ActivityRestImplService } from './service/activity-rest-impl.service';
import { ListComponent } from './activity/list/list.component';
import { LoginComponent } from './students/login/login.component';
import { ActivityAddComponent } from './activity/activity-add/activity-add.component';
import { ActivityRoutingModule } from './activity/activity-routing.module';
import { ActivityService } from './service/activity-service';
import { ActivityViewComponent } from './activity/activity-view/activity-view.component';
import { AdminComponent } from './admin/admin.component';
import { ActivityTableComponent } from './students/activity-table/activity-table.component';
import { EditProfileComponent } from './students/edit-profile/edit-profile.component';
import { TeacherService } from './service/teacher.service';
import { TeachersRestImplService } from './service/teachers-rest-impl.service';
import { ActivityEditComponent } from './activity/activity-edit/activity-edit.component';
import { EnrollTableComponent } from './students/enroll-table/enroll-table.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EnrollComponent } from './teachers/enroll/enroll.component';
import { AuthenticationService } from './service/authentication.service';
import { AuthenticationSerivceImplService } from './service/authentication-serivce-impl.service';
import { JwtInterceptorService } from './service/jwt-interceptor.service';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { CommentComponent } from './activity/comment/comment.component';
import { ApproveComponent } from './activity/approve/approve.component';
@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    HomepageComponent,
    ListComponent,
    LoginComponent,
    ActivityAddComponent,
    AdminComponent,
    ActivityTableComponent,
    EditProfileComponent,
    ActivityViewComponent,
    ActivityEditComponent,
    EnrollTableComponent,
    EnrollComponent,
    UpdateProfileComponent,
    CommentComponent,
    ApproveComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatTooltipModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    StudentRoutingModule,
    ActivityRoutingModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFileUploadModule
  ],
  providers: [
    { provide: StudentService, useClass: StudentsRestImplService },
    { provide: ActivityService, useClass: ActivityRestImplService },
    { provide: TeacherService, useClass: TeachersRestImplService },
    { provide: AuthenticationService, useClass: AuthenticationSerivceImplService },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
