import { Injectable, Output } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of, Subject } from 'rxjs';
import Student from '../entity/student';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsRestImplService extends StudentService {

  approveStudent(studentId : number, student: Student): Observable<Student>{
    console.log(`${environment.studentApi}/${studentId}`)
    return this.http.put<Student>(`${ environment.studentApi}/${student.id}`, student)
  }

  saveRequestStudent(student: Student) {
  this.enroll.push(student);
  console.log(this.enroll);
  }

  getRequestStudents():Observable<Student[]>{
    return this.http.get<Student[]>(environment.studentApi + '/status/NotApprove');
  }

  deleteStudent(student: Student): Observable<Student[]> {
    let id = this.reqStudent.indexOf(student);
    this.reqStudent.splice(id, 1);
    return of(this.reqStudent);
  }

  getStudentByActivityId(id:number):Observable<Student[]>{
    return this.http.get<Student[]>(environment.studentActivityApi +"/"+id);
  }

  getStudentEmailAndPassword(email: String, password: String): Observable<Student> {
    const output: Student = this.student.find(student => student.email === email && student.password === password);
    return of(this.student[output.id - 1]);
  }

  getStudentByMapId(studentId: String): Student {
    const output: Student = this.student.find(student => student.studentId === studentId );
    return this.student[output.id - 1];
  }

  getCheckStudentEmailAndPassword(email: String): boolean {
    const output: Student = this.student.find(student => student.email === email);
    if (output != null) {
      return true;
    } else {
      return false;
    }
  }

  constructor(private http: HttpClient) {
    super();
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi+'/'+id)
  }

  getStudentById(id: number): Student {
    return this.student[id - 1];
  }

  getEnrollStudent(id: number): Observable<Student> {
    return of(this.reqStudent[id - 1]);
  }

  // getStudent(id: number): Observable<Student> {
  //   return this.http.get<Student[]>('assets/people.json').pipe(map(students => {
  //     const output: Student = (students as Student[]).find(student => student.id === +id);
  //     return output;
  //   }));
  // }

  getStudentEmail(email: string): Observable<Student> {
    return this.http.get<Student[]>('assets/people.json').pipe(map(students => {
      const output: Student = (students as Student[]).find(student => student.email === email);
      return output;
    }));
  }

  // saveStudent(student: Student): Observable<Student> {
  //   student.id = this.student.length + 1;
  //   this.student.push(student);
  //   return of(student);
  // }

  saveEnrollStudent(enrollStudent: Student): Observable<Student> {
    enrollStudent.id = this.student.length + 1;
    this.reqStudent.push(enrollStudent);
    return of(enrollStudent);
  }

  getEnrollStudents(): Observable<Student[]> {
    return of(this.reqStudent);
  }

  saveStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(environment.studentApi, student);
  }
  

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi);
  }

  updateStudent(student: Student): Observable<Student> {
    return this.http.put<Student>(environment.studentApi, student);
  }


  enroll:Student[] = [];
  // tslint:disable-next-line:member-ordering
  student: Student[] = [{
    'id': 1,
    'studentId': '592115018',
    'name': 'Nuntapong',
    'surname': 'Lamloe',
    'date': '1997-09-18',
    // tslint:disable-next-line:max-line-length
    'image': 'https://scontent.fbkk13-1.fna.fbcdn.net/v/t1.0-9/44197029_1964156433650279_6311623929471434752_o.jpg?_nc_cat=111&_nc_eui2=AeFDA2W1-E5rid9TdDuGZFJRKXciDtja4cNOtKYwXoIU2EHd0lD0lPuVbAbqCsx37azA8yUg58YzQuT6CjG-vgDSF0Hr6NBciKjjetP1DLxBxg&oh=06f03d4194a6268de1affcc8ad29e431&oe=5C89C813',
    'email': 'nack99993@gmail.com',
    'password': '1234'
  },
  {
    'id': 2,
    'studentId': '592115000',
    'name': 'Titi',
    'surname': 'Titi',
    'date': '1997-09-18',
    // tslint:disable-next-line:max-line-length
    'image': 'https://scontent.fbkk8-3.fna.fbcdn.net/v/t1.0-9/20622079_1770517182988422_6863373371760497895_n.jpg?_nc_cat=102&_nc_eui2=AeEpEk98DscRMubu8HS3hTWe4Q4ZKwnf5Fvat07eH2fJa6qzi0LkEWPEgbSW6rcdud7edqqtPzS6f-MzNXDDWiOzvJzwUHOHm5aeGbzFKCnBsw&oh=ec0cacc10455e420595eb4022136dcec&oe=5C565473',
    'email': 'titi@gmail.com',
    'password': '1234'
  }, {
    'id': 3,
    'studentId': '592115002',
    'name': 'Frong',
    'surname': 'Fuck',
    'date': '1997-09-18',
    // tslint:disable-next-line:max-line-length
    'image': 'https://scontent.fbkk12-1.fna.fbcdn.net/v/t1.0-9/35089250_432426537217876_2679913220218552320_o.jpg?_nc_cat=101&_nc_eui2=AeEP-Htwux3pEPV3AkpnKOh_3d50KweYU6bLK08srPTPIc81n4GtUnKqpJB-6mAN1ukLmJtfyTVB_5J4TEGKbUbtGWPh_G7nWqkGx3AmxTXNLw&oh=2a8413375e12933e828bb71765a944f4&oe=5C49226F',
    'email': 'frong@gmail.com',
    'password': '1234'
  },
  {
    'id': 4,
    'studentId': '592115002',
    'name': 'Pimtha',
    'surname': 'Narak',
    'date': '1997-09-18',
    'image': 'https://f.ptcdn.info/140/027/000/1420387867-1089895882-o.jpg',
    'email': 'pimtha@gmail.com',
    'password': '1234'
  }];

  // tslint:disable-next-line:member-ordering
  reqStudent: Student[] = [];

}
