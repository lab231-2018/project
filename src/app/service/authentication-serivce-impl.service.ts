import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
import User from '../entity/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationSerivceImplService extends AuthenticationService{

  constructor(private http:HttpClient){
    super();
  }

  login(username:string,password:string){
      return this.http.post<any>(environment.authenticationApi,{
          username:username,password:password
      }).pipe(map(data => {
        console.log(data)
          if (data.token && data.user){
              localStorage.setItem('token',data.token);
              localStorage.setItem('currentUser', JSON.stringify(data.user));
              console.log(localStorage)
          }
          return data;
      }));
  }

  logout(){
      localStorage.removeItem('token');
      localStorage.removeItem('currentUser');
  }

  getToken(): string {
    const token = localStorage.getItem('token');
    return token ? token : '';
  }

  getCurrentUser(): Observable<User> {
    const currentUser = localStorage.getItem('currentUser');
    const user = JSON.parse(currentUser);
    if (currentUser) {
      // console.log('getCurrentUser:',user)
      return user;
    } else {
      return null;
    }
  }

  hasRole(role: string): boolean {
    const user: any = this.getCurrentUser();

    if (user) {
      const roleList: string[] = role.split(',');
      for (let j = 0; j < roleList.length; j++) {
        const authList = user.authorities;
        const userRole = 'ROLE_' + roleList[j].trim().toUpperCase();
        for (let i = 0; i < authList.length; i++) {
          if (authList[i].name === userRole) {
            // console.log("has role")
            return true;
          }
        }
      }
      return false;
    }
  }

  
}
