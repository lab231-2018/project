import { TestBed, inject } from '@angular/core/testing';

import { TeachersRestImplService } from './teachers-rest-impl.service';

describe('TeachersRestImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeachersRestImplService]
    });
  });

  it('should be created', inject([TeachersRestImplService], (service: TeachersRestImplService) => {
    expect(service).toBeTruthy();
  }));
});
