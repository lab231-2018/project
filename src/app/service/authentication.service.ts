import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment";
import { map } from "rxjs/operators";
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export abstract class AuthenticationService{
    abstract login(username:string,password:string);
    abstract logout();
    abstract getToken(): string;
    abstract getCurrentUser();
    abstract hasRole(role: string): boolean;



}