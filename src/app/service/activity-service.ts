import { Observable } from '../../../node_modules/rxjs';
import Activity from '../entity/activity';
import Student from '../entity/student';
import { Enroll } from '../entity/enroll';


export abstract class ActivityService {
    abstract getActivitys(): Observable<Activity[]>;
    abstract getActivity(id: number): Observable<Activity>;
    abstract saveActivity(activity: Activity): Observable<Activity>;
    abstract updateActivity(activity: Activity): Observable<Activity>;
    abstract saveEnroll(id: number);
    abstract getEnrolls(): Observable<Activity[]>;
    abstract check(id: number): boolean;
    abstract getActivityByStundetId(id: number): Observable<Activity[]>;
    abstract getActivityByLecturerId(id: number): Observable<Activity[]>;
    abstract saveEnrollActivity(studentId: number, activityId: number);
    abstract getCommentByActivityId(id: number): Observable<Activity>;
}
