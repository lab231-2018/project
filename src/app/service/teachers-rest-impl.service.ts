import { Injectable } from '@angular/core';
import { TeacherService } from './teacher.service';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import Teacher from '../entity/teacher';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeachersRestImplService extends TeacherService{
  
  getTeachers(): Observable<Teacher[]> {
    return this.http.get<Teacher[]>(environment.teacherApi);
  }
 
  getTeacher(id: number): Observable<Teacher> {
    return this.http.get<Teacher>(environment.teacherApi +"/"+id);
  }
    
  getTeacherEmailAndPassword(email: String, password: String): Observable<Teacher> {
    const output: Teacher = this.teacher.find(teacher => teacher.email === email && teacher.password === password);
    return of(this.teacher[output.id-1]);
  }


  constructor(private http: HttpClient) {
    super();
  }

  teacher:Teacher[] = [{
      "id": 1,
      "name":"Aj.A",
      "surname":"AA",
      "image":"https://cdn.aarp.net/content/dam/aarp/about_aarp/nrta/2016/1140-nrta-overall-banner-teacher-portrait.imgcache.rev3ced5b1497594481e11b88c2d0b29664.jpg",
      "email":"A",
      "password":"1234"
  },{
      "id": 2,
      "name":"Aj.B",
      "surname":"BB",
      "image":"https://wwwassets.rand.org/content/rand/capabilities/solutions/evaluating-the-effectiveness-of-teacher-pay-for-performance/jcr:content/par/solution.aspectfit.868x455.jpg/x1495314460851.jpg.pagespeed.ic.C-DXMWVcFo.jpg",
      "email":"B",
      "password":"1234"
  },{
    "id": 3,
    "name":"Aj.C",
    "surname":"CC",
    "image":"https://www.ewa.org/sites/main/files/imagecache/medium/main-images/bigstock-pretty-teacher-smiling-at-came-69887626.jpg",
    "email":"C",
    "password":"1234"
}];
}
