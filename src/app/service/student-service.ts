import { Observable } from '../../../node_modules/rxjs';
import Student from '../entity/student';

export abstract class StudentService {
  changePassword(model: Student): any {
    throw new Error("Method not implemented.");
  }
    abstract getStudents(): Observable<Student[]>;
    abstract deleteStudent(student: Student): Observable<Student[]>;
    abstract getStudent(id: number): Observable<Student>;
    abstract getStudentEmail(email: String): Observable<Student>;
    abstract getStudentEmailAndPassword(email: String, password: String): Observable<Student>;
    abstract saveStudent(student: Student): Observable<Student>;
    abstract saveEnrollStudent(student: Student): Observable<Student>;
    abstract getEnrollStudents(): Observable<Student[]>;
    abstract updateStudent(student: Student): Observable<Student>;
    abstract getCheckStudentEmailAndPassword(email: String): boolean;
    abstract getStudentById(id : number):Student;
    abstract saveRequestStudent(stundet : Student);
    abstract getRequestStudents(): Observable<Student[]>;
    abstract getStudentByMapId(stundetId:string) : Student;
    abstract approveStudent(studentId : number, student: Student): Observable<Student>;
    abstract getStudentByActivityId(id : number): Observable<Student[]>;
}
