import { Injectable } from '@angular/core';
import Activity from '../entity/activity';
import { Observable, of } from 'rxjs';
import { ActivityService } from './activity-service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from '../../../node_modules/rxjs/operators';
import { subscribeOn } from 'rxjs/operators';
import Student from '../entity/student';
import { Enroll } from '../entity/enroll';
import { StudentService } from './student-service';
import { StudentsRestImplService } from './students-rest-impl.service';
import { EnrollStatus } from '../entity/enroll-status';
import { TeacherService } from './teacher.service';


@Injectable({
  providedIn: 'root'
})
export class ActivityRestImplService extends ActivityService {

  getCommentByActivityId(id: number): Observable<Activity> {
    return this.http.get<Activity>(environment.activityApi + "/" + id);
  }

  teacher: TeacherService;
  // addStudentEnroll(student: number,activity: Activity) {
  //   console.log(student,activity);
  //   this.enrollStudent.push();
  //   console.log(this.enrollStudent);
  // }

  // getActivity(id: number): Observable<Activity> {
  //   return this.http.get<Activity[]>('assets/people.json').pipe(map(students => {
  //     const output: Activity = (students as Activity[]).find(activity => activity.id === +id);
  //     return output;
  //   }));
  // }

  getActivity(id: number): Observable<Activity> {
    return this.http.get<Activity>(environment.activityApi + "/" + id);
  }



  constructor(private http: HttpClient, private student: StudentsRestImplService) {
    super();
  }


  getActivitys(): Observable<Activity[]> {
    return this.http.get<Activity[]>(environment.activityApi);
  }

  getEnrolls(): Observable<Activity[]> {
    return of(this.enroll);
  }
  // getEnrollsById(id:number): Observable<EnrollStatus[]>{
  //   const en: EnrollStatus = this.enrollStudent.find(activity => activity.id === id);  
  //   return of(this.enroll);
  // };

  getEnrollActivitys(): Observable<EnrollStatus[]> {
    return of(this.enrollStudent);
  }

  // saveActivity(activity: Activity): Observable<Activity> {
  //   activity.id = this.activity.length + 1;
  //   this.activity.push(activity);
  //   return of(activity);
  // }

  saveActivity(activity: Activity): Observable<Activity> {
    console.log(activity)
    // addActivity.hostTeacher = this.teacher.getTeacher(activity.hostTeacher.id);
    return this.http.post<Activity>(environment.activityApi, activity);
  }


  // saveEnroll(activityId: number, studentId: string) {
  //   this.activity[activityId - 1].studentWaitingList.push(studentId + "");
  //   // for(let i = 0;i<this.activity[activityId-1].studentWaitingList.length;i++){
  //   // // console.log('student wait list', this.activity[activityId - 1].studentWaitingList[i]);
  //   console.log(this.activity[activityId - 1].studentWaitingList[0]);
  //   console.log(this.student.getStudentByMapId(this.activity[activityId - 1].studentWaitingList[0]))
  //   this.stu = this.student.getStudentByMapId(this.activity[activityId - 1].studentWaitingList[0]);
  //   this.student.saveRequestStudent(this.stu);
  //   // }
  // }

  saveEnroll(id: number) {
    this.enroll.push(this.activity[id - 1]);
    console.log(this.enroll)
  }

  saveEnrollActivity(studentId: number, activityid: number) {

    this.http.post<Activity>(environment.activityApi, activityid)
  }

  getAll(id: number) {
    let list = this.activity[id - 1].studentWaitingList;
    // list.map(this.student[id] ==> {
    //   return this.studentService.getStuden
    // })

  }
  updateActivity(activity: Activity): Observable<Activity> {
    let id = activity.id;
    this.activity[id - 1] = activity;
    console.log(this.activity);
    return of(activity);
  }

  check(id: number): boolean {
    if (this.enroll.find(activity => activity.id === id)) {
      return false;
    } else {
      return true;
    }
  }

  getEnrollInf(): Observable<Activity> {
    const act: Activity = this.activity.find(activity => activity.id === this.newEnroll.id);
    const stu: Student = this.student.student.find(student => student.id === this.newEnroll.idStudent);
    console.log(act, stu);
    return of(act)
  }
  // tslint:disable-next-line:member-ordering
  studentService: StudentService;

  getActivityByStundetId(id: number): Observable<Activity[]> {
    return this.http.get<Activity[]>(environment.activityStudentApi + "/" + id);
  }

  getActivityByLecturerId(id: number): Observable<Activity[]> {
    return this.http.get<Activity[]>(environment.activityLecturerApi + "/" + id);
  }

  approveEnroll(studentId: number, activityId: number) {
    if (this.activity[activityId].studentWaitingList.includes(studentId + "")) {
      this.activity[activityId].studentWaitingList.splice(studentId, 1);
      console.log(this.activity[activityId].studentWaitingList);
      this.activity[activityId].studentEnroll.push(studentId + '');
      console.log(this.activity[activityId].studentEnroll);
    }
  }

  // tslint:disable-next-line:member-ordering
  stu: Student;


  // tslint:disable-next-line:member-ordering
  newEnroll: EnrollStatus = {
    'id': 0,
    'idAct': 0,
    'idStudent': 0,
    'name': '',
    'location': '',
    'description': '',
    'periodOfRegis': '',
    'date': '',
    'hostTeacher': '',
    'image': '',
    'status': ''
  }
  // tslint:disable-next-line:member-ordering
  activity: Activity[] = [
    {
      "id": 1,
      "name": "Football",
      "location": "CAMT113",
      "description": "This activity for football player",
      "periodOfRegis": "18.00-20.00",
      "date": "18/09/2018",
      "lecturer": null,
      "image": "https://images-na.ssl-images-amazon.com/images/I/61F-Epj6A9L._SX355_.jpg",
      "studentWaitingList": [],
      "studentEnroll": [],
      "comment": []
    }, {
      "id": 2,
      "name": "Tennis",
      "location": "CAMT113",
      "description": "This activity for Tennis player",
      "periodOfRegis": "10.00-12.00",
      "date": "20/09/2018",
      "lecturer": null,
      "image": "https://image.freepik.com/free-icon/tennis_318-1449.jpg",
      "studentWaitingList": [],
      "studentEnroll": [],
      "comment": []
    }, {
      "id": 3,
      "name": "Basket Ball",
      "location": "CAMT219",
      "description": "This activity for Basketball player",
      "periodOfRegis": "12.00-13.00",
      "date": "20/09/2018",
      "lecturer": null,
      "image": "https://png.icons8.com/metro/1600/basketball.png",
      "studentWaitingList": [],
      "studentEnroll": [],
      "comment": []

    }, {
      "id": 4,
      "name": "Swimming",
      "location": "CAMT219",
      "description": "This activity for Swimming player",
      "periodOfRegis": "17.00-13.00",
      "date": "20/09/2018",
      "lecturer": null,
      "image": "https://cdn4.iconfinder.com/data/icons/hobbies-and-pastimes/128/11-512.png",
      "studentWaitingList": [],
      "studentEnroll": [],
      "comment": []
    }
  ];

  enroll: Activity[] = [];

  enrollStudent: EnrollStatus[] = [];
}
