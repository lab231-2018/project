import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Teacher from '../entity/teacher';

@Injectable({
  providedIn: 'root'
})
export abstract class TeacherService {
  abstract getTeachers(): Observable<Teacher[]>;
  abstract getTeacher(id: number): Observable<Teacher>;
  abstract getTeacherEmailAndPassword(email: String,password:String): Observable<Teacher>;
}
